module.exports = [
  {
    code: "en",
    name: "English",
    flag: ":flag_us:",
    translators: ["NightYoshi370#5597", "Samplasion#0325"]
  },
  {
    code: "fr",
    name: "Français (French)",
    flag: ":flag_fr:",
    translators: ["NightYoshi370#5597"]
  },
  {
    code: "it",
    name: "Italiano (Italian)",
    flag: ":flag_it:",
    translators: ["Samplasion#0325", "Miky88#3229"]
  },
  {
    code: "jp",
    name: "日本語 (Japanese)",
    flag: ":flag_jp:",
    translators: ["Pk11#1288"]
  },
  {
    code: "pl",
    name: "Polski (Polish)",
    flag: ":flag_pl:",
    translators: ["indepth6#3659"]
  },
  {
    code: "ru",
    name: "Русский (Russian)",
    flag: ":flag_ru:",
    translators: ["_Mapple2#5492"]
  },
];